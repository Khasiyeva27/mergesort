public class Main {
    public static void main(String[] args) {
        MergeSort ms = new MergeSort();
        int[] arr = {21, 56, 23, 12, 98, 5, 43};

        ms.mergeSort(arr);
        for (int element : arr) {
            System.out.print(element + " ");
        }
    }
}
